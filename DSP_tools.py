#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 14:16:17 2020

@author: s15125210
"""
import numpy as np
from matplotlib import pyplot as plt
import sounddevice as sd
from scipy import signal
import librosa



class DSP_tools(object):
    def __init__(self, fs=44100):
        self._fs = fs
        

    def create_wave(self, f, shape, dur):
        """
        f = frequecncy
        shape = waveform shape e.g. np.sin
        dur = length in seconds
        """
        ts = 1/self._fs
        t = np.arange(0,dur,ts)
        return shape(2*np.pi*f*t)
    
    
    def note(self, keynum, dur):
        A4 = 440
        ref_key = 49
        n = keynum-ref_key
        freq = A4*np.power(2,(n/12))
        return self.create_sine(freq,dur,self._fs)
    
            
    def hard_clip(self, x, amount):
        for i in range(len(wave)):
            if x[i] > amount:
                x[i] = amount
            if x[i] < -amount:
                x[i] = -amount
        return x
    
    
    def soft_clip(self, x, gain):
        return np.tanh(x*gain)
    
    
    def lfo_tool(self, x, rate):
        lfo = self.create_wave(rate, np.sin, (len(x)/self._fs))
        return x*lfo
    
    
    def beat_calc(self, bpm):
        return 60.0/float(bpm)
    
    
    def create_empty_loop(self, n_bars, bpm):
        """
        n_bars = number of bars
        """
        return np.zeros(round(self.beat_calc(bpm)*n_bars*self._fs))
    

    def fm_synth(self, carrier_f, mod_f, c_shape, m_shape ,amount, dur):
        """
        carrier_f = carrier frequency
        mod_f = modulator frequency
        c_shape = carrier waveform shape e.g np.sin
        m_shape = modulator waveform shape e.g signal.square
        amount = fm mix level
        dur = length in seconds
        """
        ts = 1.0/self._fs
        t = np.arange(0,dur, ts)
        return c_shape(2*np.pi*carrier_f*t + amount*m_shape(2*np.pi*mod_f*t))
    
    
    def convolve(self, x,impulse_path):
        [impulse,sr]=librosa.load(impulse_path,fs)
        return np.convolve(x,impulse)
        

    def ringbuffer(self, x, dur, decay=1.0):
        ''' Repeat data for 'length' amount of time, smoothing to reduce higher
            frequency oscillation. decay is the percent of amplitude decrease.
        '''
        phase = len(x)
        dur = int(self._fs * dur)
        out = np.resize(x, dur)
        for i in range(phase, dur):
            index = i - phase
            out[i] = (out[index] + out[index + 1]) * 0.5 * decay
        return out

    def tremolo(self, x, freq, shape, mix):
        dur = float(len(x)) / self._fs
        lfo = (self.create_wave(freq, shape, dur) / 2 + 0.5)
        return (x * 1-mix) + ((x * lfo) * mix)




#flanger/delay/echo(impulse-repsone and convolution)

def modulated_delay(data, modwave, dry, wet):
    ''' Use LFO "modwave" as a delay modulator (no feedback)
    '''
    out = data.copy()
    for i in range(len(data)):
        index = int(i - modwave[i])
        if index >= 0 and index < len(data):
            out[i] = data[i] * dry + data[index] * wet
    return out



def pluck(freq, length, decay=0.998, rate=44100):
    ''' Create a pluck noise at freq by sending white noise through a ring buffer
        http://en.wikipedia.org/wiki/Karplus-Strong_algorithm
    '''
    freq = float(freq)
    phase = int(rate / freq)
    data = np.random.random(phase) * 2 - 1
    return ringbuffer(data, length, decay, rate)


np.mod(np.arange(0,1,1/fs), 2 * np.pi)


#for i in range(len(empty_loop)):
#    empty_loop[i] = 


fs=44100
dsp_tools = DSP_tools(fs)


impulse_path = '/Users/s15125210/Downloads/Hesu 2x12 V30 - 440 center edge.wav'
                       
wave = dsp_tools.create_wave(80,np.sin ,1)
clip_wave = dsp_tools.hard_clip(wave, 0.7)
lfo_wave = dsp_tools.lfo_tool(clip_wave,3)
soft_c_wave = dsp_tools.soft_clip(lfo_wave,5)
fm_wave = dsp_tools.fm_synth(110, 3, signal.square, np.sin ,1, 2)

sound = dsp_tools.convolve(fm_wave, impulse)

sd.play(sound,fs)

plt.plot(soft_c_wave[:100000])




def soft_clip(x, gain):
    return np.tanh(x*gain)